#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>

using namespace std;

#define AI_WIN 1000
#define DRAW 0
#define AI_LOSE -1000

#define X 1
#define O 2

const int inf = 1000000000;
int depth = 1;

int tmpBoard[ 9 ];
int board[ 9 ];

struct data { int move; int score; };

void out( int board[] );

bool check( int tmp ) {
     
     return ( (tmpBoard[ tmp ] == 0) && tmp < 9 && tmp >= 0 );   
}

void Human() {
     
     int tmp;
     printf ("Unesi poziciju izmedju 1 i 9, ukljucivo:\n");
     scanf ("%d", &tmp);     
     printf ("\n");
     --tmp;
     
     while ( !check( tmp ) ) {
           
        system("cls");
        out ( tmpBoard );
        printf("Zauzeto ili nije izmedju 1 i 9! Pokusaj opet\n Unesi poziciju: ");
        scanf("%d", &tmp);
        --tmp;
        printf ("\n");
     } 
     
     tmpBoard[ tmp ] = X;

    return;      
}

data MaxMove( int board[] );
data MinMove( int board[] );
           
       bool IsFull( int board[] ) {
            
            bool is = true;
            for ( int i = 0; i < 9; i++ )
                if ( !board[ i ] ) is = false;    
            
            return is;
       }
       
       bool Legal( int tmp, int board[] ) {
            
            if ( board[ tmp ] == 0 && tmp < 9 && tmp >= 0 )        
               return true; 
            return false;  
       }
       
       bool Equal( int a, int b, int c, int board[] ) {     
            return ( board[ a ] == board[ b ] && board[ b ] == board[ c ] && board[ a ] == board[ c ] );     
       }
       
       int Eval( int board[] ) {
           
           if ( Equal( 0, 1, 2, board) )
              if ( board[ 0 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 0 ] == O ) return AI_WIN;
           
          if ( Equal( 0, 4, 8, board) )
              if ( board[ 0 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 0 ] == O ) return AI_WIN;
                   
           if ( Equal( 0, 3, 6, board) )
              if ( board[ 0 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 0 ] == O ) return AI_WIN;
                   
           if ( Equal( 6, 7, 8, board) )
              if ( board[ 6 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 6 ] == O ) return AI_WIN;
                   
           if ( Equal( 2, 5, 8, board) )
              if ( board[ 2 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 2 ] == O ) return AI_WIN;
                   
           if ( Equal( 3, 4, 5, board) )
              if ( board[ 3 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 3 ] == O ) return AI_WIN;
               
           if ( Equal( 1, 4, 7, board) )
              if ( board[ 1 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 1 ] == O ) return AI_WIN;
               
           if ( Equal( 6, 4, 2, board) )
              if ( board[ 6 ] == X ) 
                 return AI_LOSE; 
               else
               if ( board[ 6 ] == O ) return AI_WIN;
                   
           return DRAW;
       }
       
data NegaMax ( int board[], int PLAYER  ) {
     
     int best_score = -inf;
     int best_move;
     int EVAL = Eval( board );
     data value, best;
     
     if ( IsFull( board ) && EVAL == 0 ) {
          data best;     
          best.score = 0;
          
          return best;
     }     
     
     if ( EVAL == AI_WIN ) {  
          value.score = -AI_WIN;     
          return value;
     }
     
     if ( EVAL == AI_LOSE ) {
          value.score = AI_LOSE;     
          return value;
     }
     
     for ( int i = 0; i < 9; i++ ) {
         
         if ( !Legal( i, board ) ) continue;
         
         if ( PLAYER == X ) {
            board[ i ] = X;
            value = NegaMax( board, O );
            value.score = -value.score;
            
            if ( Eval( board ) == AI_WIN ) {
                 best.score = -AI_WIN;
                 best.move = i;
                 
                 return best;
                 }
            }
         else {
            board[ i ] = O;
            value = NegaMax( board, X );
            value.score = -value.score;
            
            if ( Eval( board ) == AI_LOSE ) {
                 best.score = AI_LOSE;
                 best.move = i;
                 
                 return best;
                 }
         }
         board[ i ] = 0;
            
         if ( value.score > best_score ) {
                   best_score = value.score;       
                   best_move = i;
              }
     }
     best.score = best_score;
     best.move = best_move;
     return best;
}
       
char convert( int x ) {
     
     if ( x == 1 ) return 'X';
     if ( x == 2 ) return 'O';      
     return '-';
}
       
void out( int board[] ) {
     
     system ("cls");
     
     printf ("Krizic Kruzic   V 1.0\n\n\n");
     
     printf ("Napomena: Ti si igrac X\n\n\n");
     
     printf ("          ");
     cout << convert( board[ 0 ] ) << " | " << convert( board[ 1 ] ) << " | " << convert( board[ 2 ] ) << endl;
     printf ("          ");
     cout << convert( board[ 3 ] ) << " | " << convert( board[ 4 ] ) << " | " << convert( board[ 5 ] ) << endl;
     printf ("          ");
     cout << convert( board[ 6 ] ) << " | " << convert( board[ 7 ] ) << " | " << convert( board[ 8 ] ) << endl; 
     printf ("\n\n");  
}
   

int main()
{       
    while ( !IsFull( tmpBoard ) ) {
          
          data help;
          out( tmpBoard );
          if ( depth % 2 != 0 ) {
             Human();
             } 
          else {
               help = NegaMax( tmpBoard, O );
               tmpBoard[ help.move ] = O;
          }
             
          if ( Eval( tmpBoard ) != 0 ) break;
          depth++;
    }
    
    out ( tmpBoard );
    if ( Eval ( tmpBoard ) == AI_WIN ) printf ("Izgubio si, hehe\n\n");
    if ( Eval ( tmpBoard ) == AI_LOSE ) printf ("Pobjedio si! Vjerojatno ce cijeli svemir nestat za par sekunda\n\n");  // xD
    if ( !Eval ( tmpBoard ) ) printf ("Nerijeseno!\n\n");
    
    system ("pause");
    return 0;
}
